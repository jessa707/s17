
	// first function here:

	function myInfo() {
		let fullName = prompt("Enter your Name: ");
		let myAge = prompt("Enter your Age: ");
		let myLocation = prompt("Enter your Location: ");
		alert("Thank you for your input");
		console.log("Hello, " + fullName);
		console.log("You are " + myAge + " years Old.");
		console.log("You live in " + myLocation);
	}


	myInfo();



/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/

	//second function here:

	function my5FavoriteBands() {
		let myBand1 = ("Black pink");
		let myBand2 = ("Taylor Swift");
		let myBand3 = ("Sean AL");
		let myBand4 = ("BTS");
		let myBand5 = ("Second Hand Serenade");
	

		console.log("1. " + myBand1);
		console.log("2. " + myBand2);
		console.log("3. " + myBand3);
		console.log("4. " + myBand4);
		console.log("5. " + myBand5);

	}

	my5FavoriteBands();

/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	
	//third function here:

	function my5FavoriteMovies() {

		let myMovie1 = ("James Bond");
		let myMovie2 = ("Riddick");
		let myMovie3 = ("Avengers");
		let myMovie4 = ("Avatar");
		let myMovie5 = ("SpiderMan");

		let theRotten = "The Rotten Tomatoes Rating: "
		

		console.log("1. " + "Movielist");
		// the Rating
		console.log(theRotten + "87%")
		console.log("2. " + myMovie2);
		// the Rating
		console.log(theRotten + "99%")
		console.log("3. " + myMovie3);
		// the Rating
		console.log(theRotten + "95%")
		console.log("4. " + myMovie4);
		// the Rating
		console.log(theRotten + "98%")
		console.log("5. " + myMovie5);
		// the Rating
		console.log(theRotten + "92%")

	}

	my5FavoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

// printUsers();

	let printFriends = function(){

	alert("Hi! Please add the names of your friends.");

	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
	};

printFriends();

// console.log(friend1);
// console.log(friend2);